<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Config;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function getLang(Request $request)
    {
        $userlang = auth()->user();
        $user_lang = $userlang->language;
        $curr_lang = $request->session()->get('locale');

        $fallback_locale = Config::get('app.fallback_locale');
        //return $request->session()->get('locale');
        if ($curr_lang == null) {
            $user1 = auth()->user();
            $id = $user1->id;
            $user = User::findOrFail($id);
            $user->language = $user_lang;
            $user->update();

            \Session::put('locale', $user_lang);

            return response()->json($curr_lang);
        } else if ($user_lang == null && $curr_lang == null) {
            $user1 = auth()->user();
            $id = $user1->id;
            $user = User::findOrFail($id);
            $user->language = $fallback_locale;
            $user->update();

            \Session::put('locale', $fallback_locale);

            return response()->json($fallback_locale);

        } else {
            return response()->json($curr_lang);
        }

    }

    public function switchLang(Request $request)
    {
        $lang = $request->lang;
        $user1 = auth()->user();
        $id = $user1->id;

        $user = User::findOrFail($id);


        $user->language = $lang;
        \Session::put('locale', $lang);
        //session(['applocale' => $lang]);
        //$request->session()->put('locale', $lang);
        //$ff = "bm";
        \App::setLocale($lang);
        $user->update();

        //return redirect()->to('/home');
        //return \Redirect::back();
    }
}
