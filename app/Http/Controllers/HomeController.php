<?php

namespace App\Http\Controllers;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $user_lang = $user->language;

        if ($user_lang != null) {

            \Session::put('locale', $user_lang);

            //return response()->json($curr_lang);
        }

        return view('home');
    }
}
