<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Language
{
    public function handle($request, Closure $next)
    {

        //$user_lang = $user->language;
        //dd($user);
        if (Session::has('locale') AND array_key_exists(Session::get('locale') /*AND (Auth::User()->get('language')!=null)*/, Config::get('app.languages'))) {
            App::setLocale(Session::get('locale'));
        } else {
            App::setLocale(Config::get('app.fallback_locale'));
        }
        return $next($request);
    }
}
