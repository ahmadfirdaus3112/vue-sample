import Vue from 'vue'
import VueRouter from "vue-router";

Vue.use(VueRouter);

let routes = [
    {path: '/dashboard', component: require('./components/Dashboard.vue')},
    /* { path: '/developer', component: require('./components/Developer.vue') },*/
    {path: '/users', component: require('./components/Users.vue')},
    {path: '/profile', component: require('./components/Profile.vue')},

    {path: '*', component: require('./components/NotFound.vue')}
];

export default new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
});
