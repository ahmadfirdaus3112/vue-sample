import Vue from "vue";
import router from "./routes";
//validate
import VeeValidate from 'vee-validate';
//gate(ACL)
import Gate from "./Gate";
//language
import VueI18n from "vue-i18n";
import Locales from "./vue-i18n-locales.generated.js";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

Vue.use(VeeValidate);

Vue.prototype.$gate = new Gate(window.user);

//pagination
Vue.component("pagination", require("laravel-vue-pagination"));

Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: "my",
    fallbackLocale: "en",
    messages: Locales
});

Vue.prototype.$locale = {
    change(lang) {
        i18n.locale = lang;
    },
    current() {
        return i18n.locale;
    }
};

//
window.Fire = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component("passport-clients", require("./components/passport/Clients.vue"));

Vue.component(
    "passport-authorized-clients",
    require("./components/passport/AuthorizedClients.vue")
);

Vue.component(
    "passport-personal-access-tokens",
    require("./components/passport/PersonalAccessTokens.vue")
);

Vue.component("not-found", require("./components/NotFound.vue"));

Vue.component(
    "example-component",
    require("./components/ExampleComponent.vue")
);

Vue.component("app", require("./components/layout/App.vue"));

const app = new Vue({
    i18n,
    el: "#app",
    router
});
